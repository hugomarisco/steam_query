**SteamQuery**

[![Build Status](https://travis-ci.org/hugomarisco/steam_query.svg?branch=master)](https://travis-ci.org/hugomarisco/steam_query)

A simple interface to Steam WebAPI and JSON endpoints.