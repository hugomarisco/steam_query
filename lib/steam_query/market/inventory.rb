require 'steam_query/market/item'

module SteamQuery
	module Market
		class Inventory

			def initialize(id64, appid)
				@id64 = id64
				@appid = appid
				@items = []
			end

			def get
				@items
			end

			def fetch
				@items.clear

				url = "http://steamcommunity.com/profiles/#{@id64}/inventory/json/#{@appid}/2/"

				response = open(url).read

				json = JSON.parse response

				json['rgInventory'].each do |id, meta|

					class_id = meta['classid']
					instance_id = meta['instanceid']

					description = json['rgDescriptions']["#{class_id}_#{instance_id}"]

					item = Item.new({
						id: id.to_i,
						class_id: class_id.to_i,
						instance_id: instance_id.to_i,
						amount: meta['amount'].to_i,
						image_url: getItemImageUrl(description['icon_url']),
						name: description['name'],
						market_name: description['market_name'],
						tradable: (description['tradable'] == 1),
						marketable: (description['marketable'] == 1),
						commodity: (description['commodity'] == 1),
						tradable_restriction: description['tradable_restriction']
					})

					description["tags"].each do |tag|
						case tag['category_name']
						when 'Type'
							item.type = tag['name']
						when 'Weapon'
							item.weapon = tag['name']
						when 'Collection'
							item.collection = tag['name']
						when 'Category'
							item.category = tag['name']
						when 'Quality'
							item.quality = tag['name']
						when 'Exterior'
							item.exterior = tag['name']
						end
					end

					@items << item
				end

				@items
			end

			private
				def getItemImageUrl(id)
					return "http://steamcommunity-a.akamaihd.net/economy/image/#{id}"
				end
		end
	end
end
