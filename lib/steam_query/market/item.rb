module SteamQuery
	module Market
		class Item

			attr_accessor :id
			attr_accessor :class_id
			attr_accessor :instance_id
			attr_accessor :amount
			attr_accessor :image_url
			attr_accessor :name
			attr_accessor :market_name
			attr_accessor :tradable
			attr_accessor :marketable
			attr_accessor :commodity
			attr_accessor :type
			attr_accessor :weapon
			attr_accessor :collection
			attr_accessor :category
			attr_accessor :quality
			attr_accessor :exterior

			def initialize(data)
				@id = data[:id]
				@class_id = data[:class_id]
				@instance_id = data[:instance_id]
				@amount = data[:amount]
				@image_url = data[:image_url]
				@name = data[:name]
				@market_name = data[:market_name]
				@tradable = data[:tradable]
				@marketable = data[:marketable]
				@commodity = data[:commodity]
				@type = data[:type]
				@weapon = data[:weapon]
				@collection = data[:collection]
				@category = data[:category]
				@quality = data[:quality]
				@exterior = data[:exterior]
			end

			def tradable?
				@tradable === true
			end

			def marketable?
				@tradable === true
			end

			def commodity?
				@tradable === true
			end
		end
	end
end