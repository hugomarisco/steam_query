module SteamQuery
	module Player
		class BanStatus
			attr_accessor :player_id
			attr_accessor :community_banned
			attr_accessor :vac_banned
			attr_accessor :vac_ban_count
			attr_accessor :days_since_last_ban
			attr_accessor :game_ban_count
			attr_accessor :economy_ban

			def initialize(data)
				@player_id = data[:player_id]
				@community_banned = data[:community_banned]
				@vac_banned = data[:vac_banned]
				@vac_ban_count = data[:vac_ban_count]
				@days_since_last_ban = data[:days_since_last_ban]
				@game_ban_count = data[:game_ban_count]
				@economy_ban = data[:economy_ban]
			end

			def vac_banned?
				@vac_banned
			end

			def community_banned?
				@community_banned
			end

			def economy_banned?
				@economy_ban != "none"
			end

			def any_active_ban?
				vac_banned? || community_banned? || economy_banned?
			end

			def any_ban_ever?
				@vac_ban_count > 0 || @game_ban_count > 0
			end

			def to_h
				{
					community_banned: @community_banned,
					vac_banned: @vac_banned,
					vac_ban_count: @vac_ban_count,
					days_since_last_ban: @days_since_last_ban,
					game_ban_count: @game_ban_count,
					economy_ban: @economy_ban
				}
			end
		end

		class Bans
      def initialize(steam_ids)
        @steam_ids = steam_ids
        @statuses = {}
      end

      def fetch
				@statuses = {}

        url = "http://api.steampowered.com/ISteamUser/GetPlayerBans/v1/?key=#{SteamQuery.api_key}&steamids=#{@steam_ids.join(',')}"

        response = open(url).read

        json = JSON.parse(response)

        json['players'].each do |player|
					@statuses[player['SteamId'].to_i] = BanStatus.new({
						player_id: player['SteamId'].to_i,
						community_banned: player['CommunityBanned'],
						vac_banned: player['VACBanned'],
						vac_ban_count: player['NumberOfVACBans'],
						days_since_last_ban: player['DaysSinceLastBan'],
						game_ban_count: player['NumberOfGameBans'],
						economy_ban: player['EconomyBan']
					})
        end

				@statuses
      end
    end
  end
end
