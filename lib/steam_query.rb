require 'open-uri'

module SteamQuery
  def self.api_key
    ENV['STEAM_API_KEY']
  end
end

require 'steam_query/market'
require 'steam_query/player'
